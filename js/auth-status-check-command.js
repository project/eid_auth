(function ($, Drupal, drupalSettings) {

  'use strict';

  /**
   * Custom ajax command for mobile ID status check.
   *
   * @param {Drupal.Ajax} ajax
   *  {@link Drupal.Ajax} object created by {@link Drupal.ajax}.
   * @param {object} response
   *   The response from the Ajax request..
   */
  Drupal.AjaxCommands.prototype.auth_status_check_command = function (ajax, response) {
    var $auth_options = $('#eid-auth-options');
    var $auth_progress = $('#eid-auth-progress');

    // We start checking authentication, hide the options and show progress.
    $auth_options.hide();
    $auth_progress.show();

    $auth_progress.find('.status-code').text(response.verification);

    var ajaxSettings = {
      url: response.path
    };

    Drupal.ajax(ajaxSettings).execute();
  };

})(jQuery, Drupal, drupalSettings);
