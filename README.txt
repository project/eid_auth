ID-Card and Mobile-ID authentication module for Drupal 8.

Recommended (tested) core version is 8.8

To get ID-Card authentication to work on Apache,
put the following into .htaccess file (or configure Apache to require certificate on path /eid/login/id_card):

# Check if SSL is enabled.
<IfModule ssl_module>
  # Initiate authentication if specific path is requested.
  <If "%{DOCUMENT_URI} == '/eid/login/id_card'">
    SSLVerifyClient require
    SSLVerifyDepth 2
  </If>
</IfModule>

NGINX doesn't allow asking SSL certificate by specific location,
so one way would be to set up second domain that points to same Drupal and
replace the ID-Card login URL with the absolute URL to the second page.

To test Mobile-ID:
WSDL url: https://tsp.demo.sk.ee/?wsdl
Service name: Testimine
Display message: Testimine

Add your certificate into test service (if you want to test
with your own phone): https://demo.sk.ee/MIDCertsReg/

Enable (on the form display settings page) field 'field_personal_id_code'
to allow entering personal ID codes.

Smart-ID needs certificates to authenticate the response from the API.
For testing purposes it is required to download the test certifications
from https://www.skidsolutions.eu/en/Repository/certs/certificates-for-testing:
"TEST of EID-SK 2016" and "TEST of EE Certification Centre Root CA" and place
them outside of Drupal root and set the path on settings page.

Identity numbers cn be found from https://github.com/SK-EID/smart-id-documentation/wiki/Environment-technical-parameters#test-accounts-for-automated-testing
