<?php
namespace Drupal\eid_auth\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

class UserLoginRouteSubscriber extends RouteSubscriberBase {

  /**
   * Alters existing routes for a specific collection.
   *
   * @param \Symfony\Component\Routing\RouteCollection $collection
   *   The route collection for adding routes.
   */
  protected function alterRoutes(RouteCollection $collection) {
    if ($route = $collection->get('user.login')) {
      // Use our controller to override default login page.
      $route->setDefault('_controller', '\Drupal\eid_auth\Controller\LoginController::login');
      $defaults = $route->getDefaults();
      unset($defaults['_form']);
      $route->setDefaults($defaults);
    }
  }

}
