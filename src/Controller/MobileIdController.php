<?php

namespace Drupal\eid_auth\Controller;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\RedirectCommand;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\TempStore\TempStoreException;
use Drupal\Core\Url;
use Drupal\Core\TempStore\PrivateTempStoreFactory;
use Drupal\eid_auth\Authentication\EidAuthentication;
use Sk\Mid\AuthenticationResponseValidator;
use Sk\Mid\DisplayTextFormat;
use Sk\Mid\Exception\MidException;
use Sk\Mid\Language\ENG;
use Sk\Mid\MobileIdClient;
use Sk\Mid\Rest\Dao\Request\AuthenticationRequest;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class EidController.
 *
 * @package Drupal\eid_auth\Controller
 */
class MobileIdController extends ControllerBase {

  const DEMO_HOST_PUBLIC_KEY_HASH = "sha256//P3c86M39KNpveCd0WJP8HiS5vrMgg84f2yHsxgYsDsI=";

  /**
   * Logger service.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * TempStoreFactory service.
   *
   * @var \Drupal\Core\TempStore\PrivateTempStoreFactory
   */
  protected $tempStoreFactory;

  /**
   * Mobile-ID controller constructor.
   *
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger
   *   Logger service.
   * @param \Drupal\Core\TempStore\PrivateTempStoreFactory $temp_store_factory
   *   Private temp store factory object.
   */
  public function __construct(LoggerChannelFactoryInterface $logger, PrivateTempStoreFactory $temp_store_factory) {
    $this->logger = $logger->get('eid_auth');
    $this->tempStoreFactory = $temp_store_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('logger.factory'),
      $container->get('tempstore.private')
    );
  }

  /**
   * Mobile-ID authentication check.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   Redirect user.
   */
  public function checkAuthentication() {
    $user = NULL;
    $config = $this->config('eid_auth.settings');
    $commands = new AjaxResponse();
    $temp_store = $this->tempStoreFactory->get('eid_auth.mobile_id');
    $authentication_hash = $temp_store->get('authentication_hash');
    $phone_number = $temp_store->get('phone_number');
    $national_identity_number = $temp_store->get('national_identity_number');
    $ssl_pinned_keys = $config->get('mobile_id_public_key_hash') ?? self::DEMO_HOST_PUBLIC_KEY_HASH;

    try {
      // Clean up the storage.
      $temp_store->delete('authentication_hash');
      $temp_store->delete('phone_number');
      $temp_store->delete('national_identity_number');
    }
    catch (TempStoreException $e) {
      $this->logger->error('Cleaning the private storage failed: @message', ['@message' => $e->getMessage()]);
    }

    $client = MobileIdClient::newBuilder()
      ->withRelyingPartyUUID($config->get('mobile_id_relying_party_uuid'))
      ->withRelyingPartyName($config->get('mobile_id_relying_party_name'))
      ->withHostUrl($config->get('mobile_id_host_url'))
      ->withLongPollingTimeoutSeconds(60)
      ->withPollingSleepTimeoutSeconds(2)
      ->withSslPinnedPublicKeys($ssl_pinned_keys)
      ->build();

    $request = AuthenticationRequest::newBuilder()
      ->withPhoneNumber($phone_number)
      ->withNationalIdentityNumber($national_identity_number)
      ->withHashToSign($authentication_hash)
      ->withLanguage(ENG::asType())
      ->withDisplayText($config->get('mobile_id_display_message'))
      ->withDisplayTextFormat(DisplayTextFormat::GSM7)
      ->build();

    try {
      $response = $client->getMobileIdConnector()->initAuthentication($request);
    }
    catch (MidException $e) {
      $this->logger->error('Mobile-ID error: @message', ['@message' => $e->getMessage()]);
      \Drupal::messenger()->addError($this->t('Unable to login with Mobile-ID!'));
      // Redirect user back to the login page.
      $commands->addCommand(new RedirectCommand(Url::fromRoute('user.login')->toString()));
      return $commands;
    }

    try {
      $final_session_status = $client
        ->getSessionStatusPoller()
        ->fetchFinalSessionStatus($response->getSessionID());

      $authentication_result = $client
        ->createMobileIdAuthentication($final_session_status, $authentication_hash);


      $resource_location = NULL;

      if ($config->get('mobile_id_resource_location')) {
        $resource_location = $config->get('mobile_id_resource_location');
      }

      if (empty($resource_location)) {
        // Default to test certificates.
        $resource_location = DRUPAL_ROOT . '/' . '../vendor/sk-id-solutions/mobile-id-php-client/tests/test_numbers_ca_certificates';
      }

      $resource_location = rtrim($resource_location, '/') . '/';

      $validator = AuthenticationResponseValidator::newBuilder()
        ->withTrustedCaCertificatesFolder($resource_location)
        ->build();

      $validator->validate($authentication_result);


      $authenticated_person = $authentication_result->constructAuthenticationIdentity();

      /* @var $user \Drupal\user\UserInterface */
      $user = EidAuthentication::findUserByPersonalIdCode($authenticated_person->getIdentityCode());

      if ($user) {
        user_login_finalize($user);
        $commands->addCommand(new RedirectCommand(eid_auth_get_login_redirect()->toString()));
      }
      else {
        $message = $this->t('Unable to login with Mobile-ID: the user with entered identity code doesn\'t exist!');
        $this->logger->error('Mobile-id error: @message', ['@message' => $message]);
        \Drupal::messenger()->addError($message);
        // Redirect user back to the login page.
        $commands->addCommand(new RedirectCommand(Url::fromRoute('user.login')->toString()));
        return $commands;
      }
    }
    catch (MidException $e) {
      $this->logger->error('Mobile-ID error: @message', ['@message' => $e->getMessage()]);
      \Drupal::messenger()->addError($this->t('Unable to log in with Mobile-ID. Please check that you have entered correct national identification number and you have a valid Mobile-ID contract.'));
      // Redirect user back to the login page.
      $commands->addCommand(new RedirectCommand(Url::fromRoute('user.login')->toString()));
      return $commands;
    }

    return $commands;
  }

}
