<?php

namespace Drupal\eid_auth\Controller;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\RedirectCommand;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Url;
use Drupal\eid_auth\Authentication\EidAuthentication;
use Drupal\Core\TempStore\PrivateTempStoreFactory;
use Sk\SmartId\Api\AuthenticationResponseValidator;
use Sk\SmartId\Api\Data\CertificateLevelCode;
use Sk\SmartId\Api\Data\Interaction;
use Sk\SmartId\Api\Data\SemanticsIdentifier;
use Sk\SmartId\Client;
use Sk\SmartId\Exception\SmartIdException;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class SmartIdController.
 *
 * @package Drupal\eid_auth\Controller
 */
class SmartIdController extends ControllerBase {

  /**
   * Logger service.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * TempStoreFactory service.
   *
   * @var \Drupal\Core\TempStore\PrivateTempStoreFactory
   */
  protected $tempStoreFactory;

  /**
   * PrivateTempStore object.
   *
   * @var \Drupal\user\PrivateTempStore
   */
  protected $store;

  /**
   * EidController constructor.
   *
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger
   *   Logger service.
   * @param \Drupal\Core\TempStore\PrivateTempStoreFactory $temp_store_factory
   *   Private temp store factory object.
   */
  public function __construct(LoggerChannelFactoryInterface $logger, PrivateTempStoreFactory $temp_store_factory) {
    $this->logger = $logger->get('eid_auth');
    $this->tempStoreFactory = $temp_store_factory;
    $this->store = $this->tempStoreFactory->get('eid_auth.smart_id');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('logger.factory'),
      $container->get('tempstore.private')
    );
  }

  /**
   * Smart-ID authentication status check.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   Redirect response.
   * @throws \Drupal\Core\TempStore\TempStoreException
   * @throws \ReflectionException
   */
  public function smartIdCheck() {
    $commands = new AjaxResponse();
    $config = $this->config('eid_auth.settings');
    $country = $config->get('smart_id_country') ?? 'EE';

    $semanticsIdentifier = SemanticsIdentifier::builder()
      ->withSemanticsIdentifierType('PNO')
      ->withCountryCode($country)
      ->withIdentifier($this->store->get('national_identity_number'))
      ->build();

    $client = new Client();
    $client->setRelyingPartyUUID($config->get('smart_id_relying_party_uuid'))
      ->setRelyingPartyName($config->get('smart_id_relying_party_name'))
      ->setHostUrl($config->get('smart_id_host_url'));

    $authenticationHash = $this->store->get('auth_hash');
    // Clean up.
    $this->store->delete('auth_hash');
    $this->store->delete('national_identity_number');

    $display_message = $config->get('smart_id_display_message') ?? $this->t('Log in');

    try {
      $authenticationResponse = $client->authentication()
        ->createAuthentication()
        ->withSemanticsIdentifier($semanticsIdentifier)
        ->withAuthenticationHash($authenticationHash)
        ->withCertificateLevel(CertificateLevelCode::QUALIFIED)
        // Todo: maybe it should be configurable?
        ->withAllowedInteractionsOrder([
          Interaction::ofTypeVerificationCodeChoice($display_message),
          Interaction::ofTypeDisplayTextAndPIN($display_message),
        ])
        ->authenticate();
    }
    catch (SmartIdException $e) {
      $this->logger->error('Smart-id exception: @type', ['@type' => get_class($e)]);

      \Drupal::messenger()->addError($this->t('Unable to log in with Smart-ID. Either the national identity number was wrong or you don\'t have an account.'));
      $commands->addCommand(new RedirectCommand(Url::fromRoute('user.login')->toString()));
      return $commands;
    }

    if (isset($authenticationResponse)) {
      $resource_location = NULL;

      if ($config->get('smart_id_resource_location')) {
        $resource_location = $config->get('smart_id_resource_location');
      }

      if (empty($resource_location)) {
        // Default to test certificates.
        $resource_location = DRUPAL_ROOT . '/' . '../vendor/sk-id-solutions/smart-id-php-client/tests/resources';
      }

      $resource_location = rtrim($resource_location, '/') . '/';

      try {
        $authenticationResponseValidator = new AuthenticationResponseValidator($resource_location);
      }
      catch (\UnexpectedValueException $exception) {
        $this->logger->error('Certificates directory is not correctly set up: @message', ['@message' => $exception->getMessage()]);
        \Drupal::messenger()->addError('Unable to log in with Smart-ID, it might not be correctly set up.');
        $commands->addCommand(new RedirectCommand(Url::fromRoute('user.login')->toString()));
        return $commands;
      }

      $authenticationResult = $authenticationResponseValidator->validate($authenticationResponse);

      // Authentication validity result.
      $isValid = $authenticationResult->isValid();

      if ($isValid) {
        $auth_identity = $authenticationResult->getAuthenticationIdentity();

        $extracted_id = EidAuthentication::smartIdextractUserPersonalIdCode($auth_identity->getIdentityCode());

        /* @var $user \Drupal\user\UserInterface */
        $user = EidAuthentication::findUserByPersonalIdCode($extracted_id);

        if ($user) {
          user_login_finalize($user);
          $commands->addCommand(new RedirectCommand(eid_auth_get_login_redirect()->toString()));
        }
        else {
          $message = $this->t('Unable to log in because the user with provided national identity code was not found.');
          $this->logger->error('Smart-id error: @message', ['@message' => $message]);
          \Drupal::messenger()->addError($message);
          $commands->addCommand(new RedirectCommand(Url::fromRoute('user.login')->toString()));
          return $commands;
        }
      }
      else {
        $errors = $authenticationResult->getErrors();
        $this->logger->error('Smart-id status: @message', ['@message' => $errors[0]]);
        \Drupal::messenger()->addError($this->t('Unable to log in because either configuration issue or the Smart-ID service experiences technical issues.'));
        $commands->addCommand(new RedirectCommand(Url::fromRoute('user.login')->toString()));

        return $commands;
      }

    }
    else {
      \Drupal::messenger()->addError($this->t('Smart-ID login failed!'));
      $this->logger->error('Smart-id status: Authentication response not set!');
    }

    return $commands;
  }

}
