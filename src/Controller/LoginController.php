<?php

namespace Drupal\eid_auth\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Url;
use Drupal\Core\PageCache\ResponsePolicy\KillSwitch;
use Drupal\eid_auth\Authentication\EidAuthentication;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Class LoginController.
 *
 * @package Drupal\eid_auth\Controller
 */
class LoginController extends ControllerBase {

  /**
   * Logger service.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * KillSwitch service.
   *
   * @var \Drupal\Core\PageCache\ResponsePolicy\KillSwitch
   */
  protected $killSwitch;

  /**
   * LoginController constructor.
   *
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger
   *   Logger service.
   * @param \Drupal\Core\PageCache\ResponsePolicy\KillSwitch $kill_switch
   *   KillSwitch service.
   */
  public function __construct(
    LoggerChannelFactoryInterface $logger, KillSwitch $kill_switch) {
    $this->logger = $logger->get('eid_auth');
    $this->killSwitch = $kill_switch;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('logger.factory'),
      $container->get('page_cache_kill_switch')
    );
  }

  /**
   * The main login page.
   *
   * @return \Symfony\Component\HttpFoundation\Response|array
   *   Redirect to user page if already logged in or
   *   authentication form render array.
   */
  public function login() {
    $user = $this->currentUser();

    if ($user->isAuthenticated()) {
      // Redirect user to user.page.
      return $this->redirect('user.page');
    }

    $login_form = $this->formBuilder()->getForm(\Drupal\user\Form\UserLoginForm::class);

    $config = $this->config('eid_auth.settings');
    $enabled_methods = $config->get('enabled_auth_methods');
    $eid_form = NULL;
    $eid_smart_id_form = NULL;

    if ($enabled_methods['mobile_id'] === 'mobile_id') {
      $eid_form = $this->formBuilder()->getForm('\Drupal\eid_auth\Form\MobileIdLoginForm');
    }

    if ($enabled_methods['smart_id'] === 'smart_id') {
      $eid_smart_id_form = $this->formBuilder()->getForm('\Drupal\eid_auth\Form\SmartIdLoginForm');
    }

    if (empty($enabled_methods) || (empty($enabled_methods['id_card']) &&
        empty($enabled_methods['mobile_id']) &&
        empty($enabled_methods['smart_id']) &&
        empty($enabled_methods['login_form']))) {
      // If nothing is enabled, show at least login form.
      $enabled_methods['login_form'] = 1;
    }

    $login_url = $config->get('id_login_url');

    if (empty($login_url)) {
      $login_url = Url::fromRoute('eid_auth.id_card');
    }

    return [
      '#theme' => 'eid_auth_login_content',
      '#forms' => [
        'id_login_url' => $login_url,
        'eid_auth_form' => $eid_form,
        'eid_auth_smart_id_form' => $eid_smart_id_form,
        'login_form' => $login_form,
      ],
      '#enabled_methods' => $enabled_methods,
      '#cache' => [
        'tags' => ['config:eid_auth.settings'],
      ],
    ];
  }

  /**
   * ID-Card authentication.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   Redirect user to page depending on authentication result.
   */
  public function idCard() {
    $this->killSwitch->trigger();

    if (!EidAuthentication::isSuccessful()) {
      \Drupal::messenger()->addError($this->t('ID-Card authentication failed.'));
      return $this->redirect('user.login');
    }
    else {
      $login = EidAuthentication::login();

      // It is possible that certificate exists but the data is in wrong format
      // or exists in other variable, so we need to handle these cases here,
      // just show an error to the user and redirect back to the login page.
      if (!$login) {
        \Drupal::messenger()->addError($this->t('ID-Card authentication failed.'));
        $this->logger->error('ID-Card authentication failed - the data provided is probably in unexpected format.');
        return $this->redirect('user.login');
      }

      return new RedirectResponse(eid_auth_get_login_redirect()->toString());
    }
  }

}
