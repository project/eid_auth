<?php

namespace Drupal\eid_auth\Authentication;

use Drupal\user\Entity\User;

/**
 * Class EidAuthentication.
 *
 * @package Drupal\eid_auth\Authentication
 */
class EidAuthentication {

  const SSL_CLIENT_VERIFY_SUCCESSFUL = 'SUCCESS';

  /**
   * {@inheritdoc}
   */
  public static function isSuccessful() {
    $ssl_verify = self::getSSLVariable('SSL_CLIENT_VERIFY');
    return $ssl_verify === self::SSL_CLIENT_VERIFY_SUCCESSFUL;
  }

  /**
   * Login the user with the ID-Card.
   */
  public static function login() {
    $ssl_client = self::getSSLVariable('SSL_CLIENT_S_DN');

    // Try second variable, if the first one didn't return anything -
    // it appears that it has two formats.
    if (!$ssl_client) {
      $ssl_client = self::getSSLVariable('SSL_CLIENT_SUBJECT_DN');
    }

    $card_info = explode('/', $ssl_client);

    if (count($card_info) <= 1) {
      $card_info = explode(',', $ssl_client);
    }

    $parameters = [];
    foreach ($card_info as $info) {
      if ($info === NULL) {
        continue;
      }
      $parameter_array = explode('=', $info);
      if (isset($parameter_array[0], $parameter_array[1])) {
        $parameters[$parameter_array[0]] = self::decodeToUtf8($parameter_array[1]);
      }
    }

    // Check if the serialNumber key exists, if not,
    // something is probably wrong here.
    if (!isset($parameters['serialNumber']))  {
      return FALSE;
    }

    // Check for newer format.
    $social_security_number = substr($parameters['serialNumber'], 0, 6) === 'PNOEE-' ? substr($parameters['serialNumber'], 6) : $parameters['serialNumber'];
    /** @var User $user */
    $user = self::findUserByPersonalIdCode($social_security_number);

    if ($user !== FALSE) {
      user_login_finalize($user);

      return TRUE;
    }

    return FALSE;
  }

  /**
   * Find user by personal id code.
   *
   * @param string $personal_id_code
   *   User personal ID code.
   *
   * @return bool|\Drupal\Core\Entity\EntityInterface
   *   User entity or false when not found.
   */
  public static function findUserByPersonalIdCode($personal_id_code) {
    $query = \Drupal::entityQuery('user');
    $query->condition('field_personal_id_code', $personal_id_code);
    // Most likely access check shouldn't be done here.
    $query->accessCheck(FALSE);
    $uids = $query->execute();

    if (empty($uids)) {
      return FALSE;
    }

    return User::load(reset($uids));
  }

  /**
   * Smart ID response returns Personal ID code in PNO{country code}-XXXXXXXXXXX format.
   * Extract only ID numbers from code.
   *
   * @param $personal_id_code
   *
   * @return bool|string|null
   */
  public static function smartIdExtractUserPersonalIdCode($personal_id_code): ?string {
    $matches = NULL;
    preg_match('/(\d{11})|(\d{6}-\d{5})/', $personal_id_code, $matches);

    $extracted_id = NULL;
    if ($matches) {
      $extracted_id = $matches[0];
    }

    return $extracted_id;
  }

  /**
   * Decodes id-card information to utf8
   *
   * @param String $str String to decode
   * @return String Decoded string
   */
  protected static function decodeToUtf8($str) {
    $str = preg_replace_callback("/\\\\x([0-9ABCDEF]{1,2})/", function ($matches) {
      return chr(hexdec($matches[1]));
    }, $str);

    $encoding = mb_detect_encoding($str, 'ASCII, UCS2, UTF8');
    if ($encoding == 'ASCII') {
      $result = mb_convert_encoding($str, 'UTF-8', 'ASCII');
    }
    else {
      if (substr_count($str, chr(0)) > 0) {
        $result = mb_convert_encoding($str, 'UTF-8', 'UCS2');
      }
      else {
        $result = $str;
      }
    }

    return $result;
  }

  /**
   * Return the SSL variable.
   *
   * The prefix of the SSL variables can be different between configurations.
   * HTTP seems to appear on NGINX/KUBERNETES platform
   * No prefix or REDIRECT appear on APACHE environment.
   *
   * @param $variable
   *
   * @return bool|mixed
   */
  protected static function getSSLVariable($variable) {
    if (isset($_SERVER['HTTP_' . $variable])) {
      return $_SERVER['HTTP_' . $variable];
    }
    elseif (isset($_SERVER['REDIRECT_' . $variable])) {
      return $_SERVER['REDIRECT_' . $variable];
    }
    elseif (isset($_SERVER[$variable])) {
      return $_SERVER[$variable];
    }

    return FALSE;
  }

}
