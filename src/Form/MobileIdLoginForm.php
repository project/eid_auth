<?php

namespace Drupal\eid_auth\Form;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\InvokeCommand;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Session\SessionManagerInterface;
use Drupal\eid_auth\Ajax\MobileIdCheckCommand;
use Drupal\Core\TempStore\PrivateTempStoreFactory;
use Sk\Mid\Exception\MidInvalidNationalIdentityNumberException;
use Sk\Mid\Exception\MidInvalidPhoneNumberException;
use Sk\Mid\MobileIdAuthenticationHashToSign;
use Sk\Mid\Util\MidInputUtil;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class MobileIdLoginForm.
 *
 * @package Drupal\eid_auth\Form
 */
class MobileIdLoginForm extends FormBase {

  /**
   * Logger service.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * TempStoreFactory service.
   *
   * @var \Drupal\Core\TempStore\PrivateTempStoreFactory
   */
  protected $tempStoreFactory;

  /**
   * SessionManager service.
   *
   * @var \Drupal\Core\Session\SessionManagerInterface
   */
  protected $sessionManager;

  /**
   * Current User object.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * EidLoginForm constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   ConfigFactory service.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger
   *   Logger service.
   * @param \Drupal\Core\TempStore\PrivateTempStoreFactory $temp_store_factory
   *   Private temp store factory object.
   * @param \Drupal\Core\Session\SessionManagerInterface $session_manager
   *   Session manager service.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   Current user account.
   */
  public function __construct(ConfigFactoryInterface $config_factory,
                              LoggerChannelFactoryInterface $logger,
                              PrivateTempStoreFactory $temp_store_factory,
                              SessionManagerInterface $session_manager,
                              AccountInterface $current_user) {
    $this->setConfigFactory($config_factory);
    $this->setLoggerFactory($logger);

    $this->tempStoreFactory = $temp_store_factory;
    $this->sessionManager = $session_manager;
    $this->currentUser = $current_user;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'eid_auth_login_form';
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('logger.factory'),
      $container->get('tempstore.private'),
      $container->get('session_manager'),
      $container->get('current_user')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['#attached']['library'][] = 'eid_auth/login';
    $form['#attached']['library'][] = 'eid_auth/auth-status-check-command';
    $form['#theme'] = 'eid_auth_login_form__mobile_id';

    $form['national_identity_number'] = [
      '#type' => 'textfield',
      '#title' => $this->t('National Identity Number'),
      '#size' => 20,
      '#required' => TRUE,
    ];

    $form['phone_number'] = [
      '#type' => 'tel',
      '#title' => $this->t('Phone Number'),
      '#placeholder' => '+37XXXXXXXXX',
      '#required' => TRUE,
      '#size' => 20,
    ];

    $form['actions'] = [
      '#type' => 'actions',
    ];

    $form['actions']['login'] = [
      '#type' => 'submit',
      '#value' => $this->t('Log in'),
      '#ajax' => [
        'callback' => '::mobileIdCallback',
        'wrapper' => 'mobile-id-login-option',
        'progress' => 'none',
      ],
    ];

    $form['#prefix'] = '<div id="mobile-id-login-option">';
    $form['#suffix'] = '</div>';

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $phone = $form_state->getValue('phone_number');
    $national_identity_number = $form_state->getValue('national_identity_number');

    // Check if the number starts with + or not, if not, just add it.
    if (strpos($phone, '+') !== 0) {
      $phone = '+' . $phone;
      $form_state->setValue('phone_number', $phone);
    }

    try {
      MidInputUtil::validatePhoneNumber($phone);
      MidInputUtil::validateNationalIdentityNumber($national_identity_number);
    }
    catch (MidInvalidPhoneNumberException $e) {
      $form_state->setErrorByName('phone_number', $this->t('The phone number is not correct!'));
    }
    catch (MidInvalidNationalIdentityNumberException $e) {
      $form_state->setErrorByName('national_identity_number', $this->t('The national identity number is not correct!!'));
    }

  }

  /**
   * Mobile-ID Ajax callback.
   *
   * @param array $form
   *   Form elements.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state object.
   *
   * @return array|\Drupal\Core\Ajax\AjaxResponse
   *   Ajax response.
   */
  public function mobileIdCallback(array &$form, FormStateInterface $form_state) {
    if ($form_state->hasAnyErrors()) {
      // Render messages only when using ajax or
      // we see duplicated messages in some cases.
      $form['status_messages'] = [
        '#type' => 'status_messages',
        '#weight' => -10,
      ];
      return $form;
    }

    $commands = new AjaxResponse();
    $config = $this->configFactory->get('eid_auth.settings');
    $phone_number = $form_state->getValue('phone_number');
    $national_identity_number = $form_state->getValue('national_identity_number');

    $authentication_hash = MobileIdAuthenticationHashToSign::generateRandomHashOfDefaultType();
    $verification_code = $authentication_hash->calculateVerificationCode();

    $temp_store = $this->tempStoreFactory->get('eid_auth.mobile_id');

    $temp_store->set('authentication_hash', $authentication_hash);
    $temp_store->set('phone_number', $phone_number);
    $temp_store->set('national_identity_number', $national_identity_number);


    if (isset($verification_code)) {
      // Remove previous error messages when there are any.
      $commands->addCommand(new InvokeCommand('.alert', 'remove'));
      $commands->addCommand(new MobileIdCheckCommand($verification_code));
    }

    return $commands;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Nothing to see here.
  }

}
