<?php

namespace Drupal\eid_auth\Form;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\InvokeCommand;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Session\SessionManagerInterface;
use Drupal\eid_auth\Ajax\SmartIdCheckCommand;
use Drupal\Core\TempStore\PrivateTempStoreFactory;
use Sk\SmartId\Api\Data\AuthenticationHash;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class SmartIdLoginForm.
 *
 * @package Drupal\eid_auth\Form
 */
class SmartIdLoginForm extends FormBase {

  /**
   * Logger service.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * TempStoreFactory service.
   *
   * @var \Drupal\Core\TempStore\PrivateTempStoreFactory
   */
  protected $tempStoreFactory;

  /**
   * PrivateTempStore object.
   *
   * @var \Drupal\user\PrivateTempStore
   */
  protected $store;

  /**
   * SessionManager service.
   *
   * @var \Drupal\Core\Session\SessionManagerInterface
   */
  protected $sessionManager;

  /**
   * Current User object.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * SmartIdLoginForm constructor.
   *
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger
   *   Logger service.
   * @param \Drupal\Core\TempStore\PrivateTempStoreFactory $temp_store_factory
   *   Private temp store factory object.
   * @param \Drupal\Core\Session\SessionManagerInterface $session_manager
   *   Session manager service.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   Current user account.
   */
  public function __construct(LoggerChannelFactoryInterface $logger,
                              PrivateTempStoreFactory $temp_store_factory,
                              SessionManagerInterface $session_manager,
                              AccountInterface $current_user) {
    $this->setLoggerFactory($logger);

    $this->tempStoreFactory = $temp_store_factory;
    $this->sessionManager = $session_manager;
    $this->currentUser = $current_user;

    $this->store = $this->tempStoreFactory->get('eid_auth.smart_id');
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'eid_auth_smart_id_login_form';
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('logger.factory'),
      $container->get('tempstore.private'),
      $container->get('session_manager'),
      $container->get('current_user')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['#attached']['library'][] = 'eid_auth/login';
    $form['#attached']['library'][] = 'eid_auth/auth-status-check-command';
    $form['#theme'] = 'eid_auth_login_form__smart_id';

    $form['national_identity_number'] = [
      '#type' => 'textfield',
      '#title' => $this->t('National Identity Number'),
      '#size' => 20
    ];

    $form['actions'] = [
      '#type' => 'actions',
    ];

    $form['actions']['smart_id_login'] = [
      '#type' => 'submit',
      '#value' => $this->t('Log in'),
      '#ajax' => [
        'callback' => '::smartIdCallback',
        'wrapper' => 'smart-id-login-option',
        'progress' => 'none',
      ],
    ];

    $form['#prefix'] = '<div id="smart-id-login-option">';
    $form['#suffix'] = '</div>';

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $national_identity_number = $form_state->getValue('national_identity_number');

    $matches = NULL;
    preg_match('/(\d{11})|(\d{6}-\d{5})/', $national_identity_number, $matches);


    if ($form_state->isValueEmpty('national_identity_number')) {
      $form_state->setErrorByName('national_identity_number', $this->t('National Identity Number must be entered!'));
    }

    if (!$matches) {
      $form_state->setErrorByName('national_identity_number', $this->t('National Identity Number is invalid!'));
    }
  }

  /**
   * Smart-ID authentication callback.
   *
   * @param array $form
   *   Form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state object.
   *
   * @return array|\Drupal\Core\Ajax\AjaxResponse
   *   Response object.
   * @throws \Drupal\Core\TempStore\TempStoreException
   */
  public function smartIdCallback(array &$form, FormStateInterface $form_state) {
    if ($form_state->hasAnyErrors()) {
      // Render messages only when using ajax or
      // we see duplicated messages in some cases.
      $form['status_messages'] = [
        '#type' => 'status_messages',
        '#weight' => -10,
      ];
      return $form;
    }

    $commands = new AjaxResponse();
    $national_identity_number = $form_state->getValue('national_identity_number');
    $authenticationHash = AuthenticationHash::generate();

    $this->store->set('auth_hash', $authenticationHash);
    $this->store->set('national_identity_number', $national_identity_number);

    $verificationCode = $authenticationHash->calculateVerificationCode();

    // Remove previous error messages when there are any.
    $commands->addCommand(new InvokeCommand('.alert', 'remove'));
    $commands->addCommand(new SmartIdCheckCommand($verificationCode));

    return $commands;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Nothing to see here.
  }

}
