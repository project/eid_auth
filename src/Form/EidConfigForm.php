<?php

namespace Drupal\eid_auth\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Path\PathValidatorInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class EidConfigForm.
 *
 * @package Drupal\eid_auth\Form
 */
class EidConfigForm extends ConfigFormBase {

  /**
   * Path validator service.
   *
   * @var \Drupal\Core\Path\PathValidator
   */
  protected $pathValidator;

  /**
   * Constructs a \Drupal\system\ConfigFormBase object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Path\PathValidatorInterface $path_validator
   *   Path validator service.
   */
  public function __construct(ConfigFactoryInterface $config_factory, PathValidatorInterface $path_validator) {
    parent::__construct($config_factory);

    $this->pathValidator = $path_validator;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('path.validator')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'eid_auth.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'eid_auth_config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('eid_auth.settings');

    $form['enabled_auth_methods'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Enabled authentication methods'),
      '#options' => [
        'id_card' => $this->t('ID-Card'),
        'mobile_id' => $this->t('Mobile-ID'),
        'smart_id' => $this->t('Smart-ID'),
        'login_form' => $this->t('Name/password login')
      ],
      '#default_value' => $config->get('enabled_auth_methods') ?: [],
    ];

    $form['mobile_id'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Mobile-ID settings'),
      '#tree' => TRUE,
    ];

    $form['mobile_id']['host_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Host URL'),
      '#default_value' => $config->get('mobile_id_host_url'),
    ];

    $form['mobile_id']['resource_location'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Resource location'),
      '#description' => $this->t('Specify the path where the certificates can be found, it must be relative to Drupal root, e.g <em>../certs</em>'),
      '#default_value' => $config->get('mobile_id_resource_location'),
    ];

    $form['mobile_id']['public_key_hash'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Public key hash'),
      '#description' => $this->t('In format: <em>sha256//hash-of-current-mid-api-ssl-host-public-key;sha256//hash-of-future-mid-api-ssl-host-public-key</em>'),
      '#default_value' => $config->get('mobile_id_resource_location'),
    ];

    $form['mobile_id']['relying_party_uuid'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Relying Party UUID'),
      '#default_value' => $config->get('mobile_id_relying_party_uuid'),
    ];

    $form['mobile_id']['relying_party_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Relying party name'),
      '#default_value' => $config->get('mobile_id_relying_party_name'),
    ];

    $form['mobile_id']['display_message'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Display message'),
      '#default_value' => $config->get('mobile_id_display_message'),
    ];

    $form['smart_id'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Smart-ID settings'),
      '#tree' => TRUE,
    ];

    $form['smart_id']['relying_party_uuid'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Relying Party UUID'),
      '#default_value' => $config->get('smart_id_relying_party_uuid'),
    ];

    $form['smart_id']['relying_party_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Relying Party Name'),
      '#default_value' => $config->get('smart_id_relying_party_name'),
    ];

    $form['smart_id']['host_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Host URL'),
      '#default_value' => $config->get('smart_id_host_url'),
    ];

    $form['smart_id']['resource_location'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Resource location'),
      '#description' => $this->t('Specify the path where the <em>trusted_certificates</em> folder can be found, it must be relative to Drupal root, e.g <em>../certs</em>'),
      '#default_value' => $config->get('smart_id_resource_location'),
    ];

    $form['smart_id']['display_message'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Display message'),
      '#default_value' => $config->get('smart_id_display_message'),
      '#size' => 60,
      '#maxlength' => 60,
    ];

    $form['smart_id']['country'] = [
      '#type' => 'select',
      '#title' => $this->t('Country'),
      '#default_value' => $config->get('smart_id_country') ?? 'EE',
      '#options' => [
        'EE' => $this->t('Estonia'),
        'LV' => $this->t('Latvia'),
        'LT' => $this->t('Lithuania'),
      ],
    ];

    $form['login_redirect'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Redirect path'),
      '#description' => $this->t('Path to redirect after successful login.'),
      '#default_value' => $config->get('login_redirect'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);

    $redirect_path = $form_state->getValue('login_redirect');

    if (!empty($redirect_path)) {
      $url = $this->pathValidator->getUrlIfValidWithoutAccessCheck($redirect_path);

      if (!$url) {
        $form_state->setErrorByName('login_redirect', 'Redirect path must exist!');
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('eid_auth.settings')
      // Mobile-ID configuration save.
      ->set('mobile_id_host_url', trim($form_state->getValue(['mobile_id', 'host_url'])))
      ->set('mobile_id_resource_location', $form_state->getValue(['mobile_id', 'resource_location']))
      ->set('mobile_id_public_key_hash', $form_state->getValue(['mobile_id', 'public_key_hash']))
      ->set('mobile_id_relying_party_uuid', $form_state->getValue(['mobile_id', 'relying_party_uuid']))
      ->set('mobile_id_relying_party_name', $form_state->getValue(['mobile_id', 'relying_party_name']))
      ->set('mobile_id_display_message', $form_state->getValue(['mobile_id', 'display_message']))
      // SmartID configuration save.
      ->set('smart_id_relying_party_uuid', $form_state->getValue(['smart_id', 'relying_party_uuid']))
      ->set('smart_id_relying_party_name', $form_state->getValue(['smart_id', 'relying_party_name']))
      ->set('smart_id_host_url', $form_state->getValue(['smart_id', 'host_url']))
      ->set('smart_id_resource_location', $form_state->getValue(['smart_id', 'resource_location']))
      ->set('smart_id_display_message', $form_state->getValue(['smart_id', 'display_message']))
      ->set('smart_id_country', $form_state->getValue(['smart_id', 'country']))
      // Enabled auth methods save.
      ->set('enabled_auth_methods', $form_state->getValue('enabled_auth_methods'))
      // Redirect save.
      ->set('login_redirect', $form_state->getValue('login_redirect'))
      ->save();
  }

}
